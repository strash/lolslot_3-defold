varying mediump vec4 position;
varying mediump vec2 var_texcoord0;

uniform lowp sampler2D texture_sampler;
uniform lowp vec4 tint;
uniform lowp vec4 blur;


void main() {
	lowp vec4 col = texture2D( texture_sampler, var_texcoord0.xy );
	if (blur.x > 0.0) {
		for (int i = 0; i < int(blur.x); i++) {
			float y = var_texcoord0.y + float(i) * 0.02 - 0.375;
			col = (col + texture2D( texture_sampler, vec2(var_texcoord0.x, y) )) * 0.5;
		}
	}

	gl_FragColor = col;
}
