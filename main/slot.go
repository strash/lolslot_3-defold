embedded_components {
  id: "sprite"
  type: "sprite"
  data: "tile_set: \"/main/slots.atlas\"\n"
  "default_animation: \"ic_1_1\"\n"
  "material: \"/material/slot.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
